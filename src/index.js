import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { calculateWinner } from './helper';

function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  
  renderSquare(i) {
    return <Square value={this.props.squares[i]} onClick={() => this.props.onClick(i)}/>;
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor() {
    super();
    this.state = {
      history: [{
        squares: Array(9).fill(''),
        move: 'o'
      }],
      step: 0
    }
  }

  getCurrent() {
    const history = this.state.history;
    return history[this.state.step];
  }

  getMove() {
    return this.getCurrent().move === 'o' ? 'x' : 'o'
  }

  jumpTo(step) {
    this.setState({ step })
  }

  handleClick(i) {
    const squares = [...this.getCurrent().squares];
    if (squares[i] || calculateWinner(squares) ) {
      return;
    }
    squares[i] = this.getCurrent().move;
    this.setState({ 
      history: [...this.state.history, { squares, move: this.getMove() }],
      step: this.state.history.length
    })
  }

  render() {
    const squares = this.getCurrent().squares;
    const winner = calculateWinner(squares);
    const status = winner ? `${winner} won` : `Next player: ${this.props.move}`;
    const moves = this.state.history.map((step, i) => {
      return (
        <li>
          <button onClick={() => this.jumpTo(i)}>
            {`Go to move ${i}`}
          </button>
        </li>
      )
    })

    return (
      <div className="game">
        <div className="game-board">
          <Board squares={squares} onClick={(i) => this.handleClick(i)}/>
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
